package com.example.jacob.a3dmodelviewer;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by Jacob on 5/09/2016.
 */
class MyGLSurfaceView extends GLSurfaceView {

    private final GLRenderer mRenderer;

    public MyGLSurfaceView(Context context){
        super(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new GLRenderer(context);

        // Set the Renderer for drawing on the MyGLSurfaceView
        setRenderer(mRenderer);
    }
}