package com.example.jacob.a3dmodelviewer;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Jacob on 6/09/2016.
 */
public class ObjParser {

    private String[] fileStringArray;
    private float[] vertexPositions;
    private float modelScale;
    private float[] centre;
    private float[] minValues;
    private float[] MaxValues;

    public ObjParser(String fileDir){

    // TODO read in file and get the vertices
    // vertex normals, bounding box, centre, number of vertices
    // if time, texture coordinates

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileDir);
        }
        catch ( FileNotFoundException e) {
            e.printStackTrace();
            Log.d("JDEBUG",e.toString());
        }

        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        int numLines = 0;
        try{
            while (br.readLine() != null)
                numLines++;
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try{
            fis.getChannel().position(0);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        this.fileStringArray = new String[numLines];

        String line;
        int i = 0;
        try{
            while((line=br.readLine())!=null){
                this.fileStringArray[i] = line;
                i++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
