package com.example.jacob.a3dmodelviewer;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Jacob on 5/09/2016.
 */
public class Cube {
    private FloatBuffer vertexBuffer;

    private int posBufferId;
    private int colBufferId;
    private int mMVPMatrixHandle;



    private final int vertexCount = triangleCoords.length / COORDS_PER_VERTEX;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float triangleCoords[] = {   // in counterclockwise order:
        1.0f,1.0f,1.0f,
        -1.0f,-1.0f,1.0f,
        -1.0f,1.0f,1.0f,

        1.0f,1.0f,1.0f,
        1.0f,-1.0f,1.0f,
        -1.0f,-1.0f,1.0f,

        1.0f,1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,1.0f,-1.0f,

        1.0f,1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,

        -1.0f,1.0f,1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,1.0f,

       -1.0f,1.0f,1.0f,
       -1.0f,1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,

        1.0f,1.0f,1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,1.0f,

        1.0f,1.0f,1.0f,
        1.0f,1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,

        -1.0f,1.0f,1.0f,
        -1.0f,1.0f,-1.0f,
        1.0f,1.0f,1.0f,

        1.0f,1.0f,1.0f,
        1.0f,1.0f,-1.0f,
       -1.0f,1.0f,-1.0f,

        -1.0f,-1.0f,1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,1.0f,

        1.0f,-1.0f,1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f
    };

    // Set color with red, green, blue and alpha (opacity) values
    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };

    public Cube(int program) {

        posBufferId = GLES20.glGetAttribLocation(program, "vPosition");
        colBufferId = GLES20.glGetUniformLocation(program, "vColor");
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                triangleCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(triangleCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);
    }

    public void draw(float[] mvpMatrix, int program) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(program);

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(posBufferId);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(posBufferId, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(GLES20.glGetUniformLocation(program, "uMVPMatrix"), 1, false, mvpMatrix, 0);

        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(posBufferId);

        // Set color for drawing the triangle
        GLES20.glUniform4fv(colBufferId, 1, color, 0);

        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(posBufferId);
    }


}
