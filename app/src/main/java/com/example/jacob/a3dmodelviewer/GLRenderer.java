package com.example.jacob.a3dmodelviewer;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Environment;
import android.os.SystemClock;
import android.content.Context;
import android.util.Log;

import java.io.File;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Jacob on 5/09/2016.
 */
public class GLRenderer implements GLSurfaceView.Renderer {

    private Triangle mTriangle;
    private Cube mCube;
    private Plane mPlane;
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private float[] mRotationMatrix = new float[16];
    private int program = 0;
    private Context context;

    String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
            "attribute vec4 vPosition;" +
            "void main() {" +
            "   gl_Position = uMVPMatrix * vPosition;" +
            "}";

    String fragmentShaderCode =
            "precision mediump float;" +
            "uniform vec4 vColor;" +
            "void main() {" +
            "   gl_FragColor = vColor;" +
            "}";

    public GLRenderer(Context context){
        this.context = context;
    }


    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        program = initShaders(vertexShaderCode,fragmentShaderCode);

        // initialize a triangle
        mTriangle = new Triangle();
        mPlane = new Plane();
        mCube = new Cube(program);

        /*
        //File file;
        // check if the app can access the phones storage.
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            //file = new File();
            ObjParser temp = new ObjParser(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator + "cube.obj");
        } else {
            Log.d("JDEBUG","Cannot Read from external Strorage");

        }*/
    }

    // render code?
    public void onDrawFrame(GL10 unused) {
        float[] scratch = new float[16];
        // Redraw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

        // Create a rotation transformation for the triangle
        long time = SystemClock.uptimeMillis() % 4000L;
        float angle = 0.090f * ((int) time);
        Matrix.setRotateM(mRotationMatrix, 0, angle, 1.0f, 0.1f, -1.0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
        Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mRotationMatrix, 0);

        mPlane.draw(mMVPMatrix, program);
        mCube.draw(scratch, program);

        //mTriangle.draw(scratch, program);
    }

    // resize window?
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 1.0f, 10);
    }


    // Creates and compiles the shader code.
    public static int loadShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    // Create the shader program
    public int initShaders(String vertexShaderCode, String fragmentShaderCode) {

        int vertexShader = GLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = GLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        // create empty OpenGL ES Program
        int shaderProgram;
        shaderProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(shaderProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(shaderProgram, fragmentShader);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(shaderProgram);

        return shaderProgram;
    }

}